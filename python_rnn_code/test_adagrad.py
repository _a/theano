from utils import lookup
from utils import getWordmap
from params import params
from utils import getData
from adagrad import adagrad
import warnings
import numpy as np
import random

np.random.seed(1)
random.seed(1)

hiddensize = 25
(words, We) = getWordmap('../data/skipwiki25.txt')
params = params()
examples = getData(params.dataf)
sample = examples[0:100]
#(words,We) = limitwords(words,We,sample)

print "Testing adagrad"
params.data = sample
params.epochs=20
params.eta=0.20
params.batchsize=75
adagrad(params,words,We)