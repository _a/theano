import pdb
import numpy as np

def lookup(We,words,w):
    w = w.lower()
    if w in words:
        return We[words[w],:]
    else:
        return We[words['UUUNKKK'],:]

def sigmoid(x):
    result = 1 / (1 + np.exp(-x))
    return result

class tree(object):

    def __init__(self, phrase):

        self.phrase = phrase
        self.representation = None
        self.embeddings = []

    def getEmbeddings(self, We, words):
        arr = self.phrase.split()
        for s in arr:
            self.embeddings.append(lookup(We, words, s))

    def feedforward(self, Wi, bi, W1, W2, b, We, words):
        self.getEmbeddings(We,words)
        ct = 1
        #pdb.set_trace()
        #print Wi.eval().shape, self.embeddings[0].eval().shape, bi.eval().shape
        prev = np.dot(Wi,self.embeddings[0]) + bi
        #print T.dot(Wi, self.embeddings[0]).eval().shape
        prev = sigmoid(prev)
        #pdb.set_trace()
        while ct < len(self.embeddings):
            new = np.dot(W1,prev) + np.dot(W2,self.embeddings[ct]) + b
            new = sigmoid(new)
            #print ct, prev.eval().shape
            prev = new
            ct += 1
        self.representation = prev