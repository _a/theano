
from utils import *
from params import params
from theano_model import theano_model
from adagrad import adagrad
import random
import numpy as np

random.seed(1)
np.random.seed(1)

params = params()

params.lamWe=1E-3
params.dropout = 0.5
params.frac = 0.1
params.outfile = 'test.out'
params.batchsize = 100
params.hiddensize = 25
params.type = "MAX"
params.save = True

(words, We) = getWordmap(params.wordfile)
wordfilestem = params.wordfile.split("/")[-1].replace(".txt","")
params.outfile = "../models/"+params.outfile+"."+str(params.lamWe)+"."+str(params.L_C)+"."+str(params.batchsize)+"."+params.type+\
                 "."+wordfilestem+".txt"
examples = getData(params.dataf,words)
word_pairs = getWordPairs('../data/ppdb-new.2.txt',words)
examples = convertPairsToExamples(examples, word_pairs)

params.data = examples[0:int(params.frac*len(examples))]

print "Training on "+str(len(params.data))+" examples using lambda="+str(params.lamWe)\
      +" and dropout rate of: "+str(params.dropout)
print "Saving to: "+params.outfile

adagrad(params, words, theano_model(We,params.hiddensize,params.L_C))
