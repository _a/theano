
class params(object):

    def __init__(self, lamWe=1E-5, lamC=1E-5, dataf='../data/phrase_pairs',
        batchsize=100, margin=1, epochs=20, eta = 0.10, evaluate=True, save=False,
        hiddensize=25, outfile="test.out", type = "MAX"):

        self.lamWe=lamWe
        self.lamC=lamC
        self.dataf = dataf
        self.batchsize = batchsize
        self.margin = margin
        self.epochs = epochs
        self.eta = eta
        self.evaluate = evaluate
        self.save = save
        self.data = []
        self.hiddensize = hiddensize
        self.outfile = outfile
        self.type = type
