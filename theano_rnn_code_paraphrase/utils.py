from scipy.io import loadmat
import numpy as np
from tree import tree
from random import shuffle
from random import randint
from random import choice

def lookup(We,words,w):
    w = w.lower()
    if w in words:
        return We[words[w],:]
    else:
        return We[words['UUUNKKK'],:]

def lookupIDX(words,w):
    w = w.lower()
    if w in words:
        return words[w]
    else:
        return words['UUUNKKK']

def lookup_with_unk(We,words,w):
    w = w.lower()
    if w in words:
        return We[words[w],:],False
    else:
        return We[words['UUUNKKK'],:],True

#changed
def getData(f,words):
    data = open(f,'r')
    lines = data.readlines()
    examples = []
    for i in lines:
        i=i.strip()
        if(len(i) > 0):
            i=i.split('|||')
            e = (tree(i[0], words), tree(i[1], words))
            examples.append(e)
    return examples

def getWordmap(textfile):
    words={}
    We = []
    f = open(textfile,'r')
    lines = f.readlines()
    for (n,i) in enumerate(lines):
        i=i.split()
        j = 1
        v = []
        while j < len(i):
            v.append(float(i[j]))
            j += 1
        words[i[0]]=n
        We.append(v)
    return (words, np.array(We))


def getPairMax(d,idx,v):
    min = -5000
    best = None
    for i in range(len(d)):
        if i == idx:
            continue
        (t1,t2) = d[i]
        v1 = t1.representation
        v2 = t2.representation
        np1 = np.inner(v1,v)
        np2 = np.inner(v2,v)
        if(np1 > min):
            min = np1
            best = t1
        if(np2 > min):
            min = np2
            best = t2
    return best

def getPairRand(d,idx):
    wpick = None
    ww = None
    while(wpick == None or (idx == ww)):
        ww = choice(d)
        ridx = randint(0,1)
        wpick = ww[ridx]
    return wpick

def getPairMix(d,idx,v):
    r1 = randint(0,1)
    if r1 == 1:
        return getPairMax(d,idx,v)
    else:
        return getPairRand(d,idx)

def getPairs(d, type, model):
    pairs = []
    model.feedforwardAll(d)
    for i in range(len(d)):
        (t1,t2) = d[i]
        v1 = t1.representation
        v2 = t2.representation
        p1 = None
        p2 = None
        if type == "MAX":
            p1 = getPairMax(d,i,v1)
            p2 = getPairMax(d,i,v2)
        if type == "RAND":
            p1 = getPairRand(d,i)
            p2 = getPairRand(d,i)
        if type == "MIX":
            p1 = getPairMix(d,i,v1)
            p2 = getPairMix(d,i,v2)
        pairs.append((p1,p2))
    return pairs

def getPairsBatch(d, batchsize, type, model):
    idx = 0
    pairs = []
    while idx < len(d):
        batch = d[idx: idx + batchsize if idx + batchsize < len(d) else len(d)]
        if(len(batch) <= 2):
            print "batch too small."
            continue #just move on because pairing could go faulty
        p = getPairs(batch, type, model)
        pairs.extend(p)
        idx += batchsize
    return pairs

def getTuplesFromExamples(d,p):
    d1 = []
    d2 = []
    for i in d:
        (t1,t2) = (i[0].embeddings, i[1].embeddings)
        d1.append(t1)
        d2.append(t2)
    return d1,d2

def convertTextToTuples(s1, s2, words):
    X1 = []
    X2 = []
    for i in s1:
        i = i.split()
        lis = []
        for j in i:
            lis.append(lookupIDX(words,j))
        X1.append(lis)
    for i in s2:
        i = i.split()
        lis = []
        for j in i:
            lis.append(lookupIDX(words,j))
        X2.append(lis)
    return X1, X2

def loadSE(f):
    f = open(f,'r')
    lines = f.readlines()
    d1 = []; d2 = []; scores= []; ents= []
    for (n,i) in enumerate(lines):
        if n ==0:
            continue
        i = i.split('\t')
        s1 = i[1]
        s2 = i[2]
        score = float(i[3])
        entailment = i[4]
        d1.append(s1); d2.append(s2); scores.append(score); ents.append(entailment)
    return d1,d2,scores,ents

