
from utils import getWordmap
from params import params
from utils import getData
from theano_lstm_model import theano_lstm_model
from adagrad import adagrad
import random
import numpy as np

random.seed(1)
np.random.seed(1)

params = params()

params.lamWe=1E-2
params.dropout = 0
params.frac = 0.5
params.outfile = 'test.out'
params.batchsize = 500
params.hiddensize = 25
params.type = "MAX"
params.save = True

(words, We) = getWordmap(params.wordfile)
wordfilestem = params.wordfile.split("/")[-1].replace(".txt","")
params.outfile = "../models/"+params.outfile+"."+str(params.lamWe)+"."+str(params.dropout)+"."+str(params.batchsize)+"."+params.type+\
                 "."+wordfilestem+".txt"
examples = getData(params.dataf,words)

params.data = examples[0:int(params.frac*len(examples))]

print "Training on "+str(len(params.data))+" examples using lambda="+str(params.lamWe)\
      +" and dropout rate of: "+str(params.dropout)
print "Saving to: "+params.outfile

adagrad(params, words, theano_lstm_model(We,params.hiddensize,params.dropout))
