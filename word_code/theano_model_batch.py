from collections import OrderedDict
import numpy
from scipy.spatial.distance import cosine
import theano
import random
from evaluate import evaluate_adagrad
from random import shuffle
import theano.tensor as tensor
import time
from utils import *
import sys
import optimizers


def build_embedding_function(params):
    We = params["We"]
    x1 = tensor.matrix('x1',dtype='int64')
    emb1 = We[x1]
    emb1 = emb1.reshape([x1.shape[0],We.shape[1]])
    ff = theano.function([x1],emb1,name="embedding")
    return ff

def build_model(tparams,D,margin,reg,We_init,g1,g2,p1,p2,ll):
    We = tparams["We"]
    embg1 = We[g1.flatten()].reshape([g1.shape[0],D])
    embg2 = We[g2.flatten()].reshape([g2.shape[0],D])

    embp1 = We[p1.flatten()].reshape([p1.shape[0],D])
    embp2 = We[p2.flatten()].reshape([p2.shape[0],D])

    g1g2 = (embg1*embg2).sum(axis=1)
    g1g2norm = tensor.sqrt(tensor.sum(embg1**2,axis=1)) * tensor.sqrt(tensor.sum(embg2**2,axis=1))
    g1g2 = g1g2 / g1g2norm

    p1g1 = (embp1*embg1).sum(axis=1)
    p1g1norm = tensor.sqrt(tensor.sum(embp1**2,axis=1)) * tensor.sqrt(tensor.sum(embg1**2,axis=1))
    p1g1 = p1g1 / p1g1norm

    p2g2 = (embp2*embg2).sum(axis=1)
    p2g2norm = tensor.sqrt(tensor.sum(embp2**2,axis=1)) * tensor.sqrt(tensor.sum(embg2**2,axis=1))
    p2g2 = p2g2 / p2g2norm

    costp1g1 = margin - ll*g1g2 + p1g1
    costp1g1 = costp1g1*(costp1g1 > 0)

    costp2g2 = margin - ll*g1g2 + p2g2
    costp2g2 = costp2g2*(costp2g2 > 0)

    cost = costp1g1 + costp2g2

    cost =  tensor.mean(cost) + reg/2.*((We-We_init)**2).sum()
    #return reg/2.*((We-We_init)**2).sum()
    return cost

def python_cost(margin,reg,We_init,g1,g2,p1,p2,ll,params):
    We = params["We"]
    [_,D] = We.shape
    #print x1
    embg1 = We[g1.flatten()].reshape(g1.shape[0],D)
    embg2 = We[g2.flatten()].reshape(g2.shape[0],D)

    embp1 = We[p1.flatten()].reshape(p1.shape[0],D)
    embp2 = We[p2.flatten()].reshape(p2.shape[0],D)
    #print emb1, emb1.shape
    vals = []
    for i in range(embg1.shape[0]):
        g1 = embg1[i,:]
        g2 = embg2[i,:]
        p1 = embp1[i,:]
        p2 = embp2[i,:]
        cg1g2 = -cosine(g1,g2)+1
        cg1p1 = -cosine(g1,p1)+1
        cg2p2 = -cosine(g2,p2)+1
        c = numpy.maximum(margin - ll[i]*cg1g2 + cg1p1,0.) + numpy.maximum(margin - ll[i]*cg1g2 + cg2p2,0.)
        #c = margin - cg1g2 + cg1p1 + margin - cg1g2 + cg2p2
        #print margin - cg1g2 + cg1p1, margin - cg1g2 + cg2p2
        #c = v1*v2
        #c = c.sum()
        vals.append(c)
    return numpy.mean(vals) + reg/2.*((We-We_init)**2).sum()
    #return reg/2.*((We-We_init)**2).sum()
    #return vals

def init_params_word(D,V):
    params = OrderedDict()
    We = numpy.random.uniform(low=-2, high = 2, size=(V,D)).astype(theano.config.floatX)
    params["We"] = We
    return params

def init_tparams_word(params):
    tparams = OrderedDict()
    for kk, pp in params.iteritems():
        tparams[kk] = theano.shared(params[kk], name=kk)
    return tparams

def checkIfQuarter(idx,n,batchsize):
    start = idx - batchsize
    for i in range(start,idx):
        if i==round(n/4.) or i==round(n/2.) or i==round(3*n/4.):
            return True
    return False

def saveParams(words,We,outfile,counter):
    #save as text file for now
    outfile = outfile + ".params."+str(counter)+".txt"
    fout = open(outfile,'w')
    for i in words:
        word = i
        vec = We[words[i],:]
        ss = word+"\t"
        for i in vec:
            ss = ss + str(i)+"\t"
        ss=ss.strip()
        fout.write(ss+"\n")
    fout.close()

def evaluateParams(words,We):
    evaluate_adagrad(We,words)

def train(options, words, We):
    #We = theano.config.floatX(We)
    data = options.data
    n = len(data)
    counter = 0
    optimizer=optimizers.adagrad
    params = OrderedDict()
    params["We"] = We
    We_init = theano.shared(We,name="We_init")
    tparams = init_tparams_word(params)

    #build model
    g1 = tensor.vector('g1',dtype='int64')
    g2 = tensor.vector('g2',dtype='int64')
    p1 = tensor.vector('p1',dtype='int64')
    p2 = tensor.vector('p2',dtype='int64')
    ll = tensor.vector('ll',dtype='int64')
    cost = build_model(tparams,We.shape[1],options.margin,options.lam,We_init,g1,g2,p1,p2,ll)

    f_cost = theano.function([g1,g2,p1,p2,ll], cost, name='cost')
    grads = tensor.grad(cost, wrt=tparams.values())
    f_update = optimizer(grads, tparams.values(), [g1,g2,p1,p2,ll], options.eta)

    for ep in range(options.epochs):
        shuffle(data)
        We =  tparams["We"].get_value()
        #t1 = time.time()
        pairs = getPairsBatch(data,words,We,options.batchsize,options.type)
        #t2 = time.time()
        #print t2 - t1, len(pairs)
        newp = [convertToIndex(i,words,We) for i in pairs]
        newd = [convertToIndex(i,words,We) for i in data]
        g1,g2,p1,p2,ll = makeBatch(newd,newp)
        #print g1,g2,p1,p2
        cost = f_cost(g1,g2,p1,p2,ll)
        print "cost at epoch at "+str(ep)+" : "+str(cost)
        idx = 0
        while idx < n:
            tt1 = time.time()
            batch = data[idx: idx + options.batchsize if idx + options.batchsize < len(data) else len(data)]
            idx += options.batchsize
            if(len(batch) <= 2):
                print "batch too small."
                continue #just move on because pairing could go faulty
            #print idx
            t1 = time.time()
            pairs = getPairs(batch,words,We,options.type)
            t2 = time.time()
            #print "pairing: ", t2 -t1
            newp = [convertToIndex(i,words,We) for i in pairs]
            newd = [convertToIndex(i,words,We) for i in batch]
            g1,g2,p1,p2,ll = makeBatch(newd,newp)
            t1 = time.time()
            #f_grad_shared(g1,g2,p1,p2,ll)
            f_update(g1,g2,p1,p2,ll)
            t2 = time.time()
            #print "grad: ", t2-t1
            We = tparams["We"].get_value()
            if(checkIfQuarter(idx,n,options.batchsize)):
                if(options.save):
                    counter += 1
                    saveParams(words,We,options.outfile,counter)
                if(options.evaluate):
                    evaluateParams(words,We)
                    sys.stdout.flush()
            tt2 = time.time()
            #print "time for batch: ", tt2 - tt1
        if(options.save):
            counter += 1
            saveParams(words,We,options.outfile,counter)
        if(options.evaluate):
            evaluateParams(words,We)
            sys.stdout.flush()

def main():

    #make data
    N = 100
    batchsize = 10
    D = 5
    L = 1
    V = 100
    margin = 1
    reg = 1E-2
    eta = 0.05
    optimizer=sgd
    data=[]

    labels = [1,-1]

    for i in range(N/batchsize):
        g1 = numpy.random.randint(V,size=(batchsize,L))
        g2 = numpy.random.randint(V,size=(batchsize,L))
        p1 = numpy.random.randint(V,size=(batchsize,L))
        p2 = numpy.random.randint(V,size=(batchsize,L))
        ll = [random.choice(labels) for i in range(batchsize)]
        data.append((i,g1,g2,p1,p2,ll))

    params = init_params_word(D,V)
    tparams = init_tparams_word(params)

    We_init = params["We"]
    We_init = numpy.zeros(We_init.shape)

    #build model
    g1 = tensor.matrix('g1',dtype='int64')
    g2 = tensor.matrix('g2',dtype='int64')
    p1 = tensor.matrix('p1',dtype='int64')
    p2 = tensor.matrix('p2',dtype='int64')
    ll = tensor.vector('ll',dtype='int64')
    cost = build_model(tparams,D,margin,reg,We_init,g1,g2,p1,p2,ll)

    f_cost = theano.function([g1,g2,p1,p2,ll], cost, name='cost')
    grads = tensor.grad(cost, wrt=tparams.values())
    f_update = optimizer(grads, tparams, eta)

    #ef = build_embedding_function(params)

    #print data[0][1]
    t1 = time.time()
    for k in range(10):
        for i in data:
            #print i[0]
            #v = f_cost()
            params["We"] = tparams["We"].get_value()
            v = f_cost(i[1],i[2],i[3],i[4],i[5])
            c = python_cost(margin,reg,We_init,i[1],i[2],i[3],i[4],i[5],params)
            print v,c, v - c
            #print v
            #f_grad_shared(i[1],i[2],i[3],i[4],i[5])
            f_update()
    t2 = time.time()
    print t2-t1

if __name__ == "__main__":
    main()