
import theano
import numpy as np
from theano import tensor as T
from theano.ifelse import ifelse

class theano_word_model(object):

    def __init__(self, We_initial,params):
        self.initial_We = theano.shared(We_initial)
        self.We = theano.shared(We_initial)
        self.We_ada = theano.shared(value=np.zeros(We_initial.shape, dtype=theano.config.floatX))
        self.gg = T.dmatrix()
        self.idx1 = T.ivector()
        self.idx2 = T.ivector()
        self.zero = theano.shared(0.)
        self.lam = T.dscalar()
        self.eta = T.dscalar()
        self.n = T.dscalar()
        self.label = T.dscalar()

        self.x1 = self.We[self.idx1[0],:]
        self.x2 = self.We[self.idx1[1],:]
        self.t1 = self.We[self.idx2[0],:]
        self.t2 = self.We[self.idx2[1],:]

        self.nx1 = T.sqrt(T.sum(self.x1**2))
        self.nx2 = T.sqrt(T.sum(self.x2**2))
        self.nt1 = T.sqrt(T.sum(self.t1**2))
        self.nt2 = T.sqrt(T.sum(self.t2**2))

        #self.s1 = 1-self.label*T.dot(self.x1,self.x2)+T.dot(self.t1,self.x1)
        #self.s2 = 1-self.label*T.dot(self.x1,self.x2)+T.dot(self.t2,self.x2)

        self.s1 = 1-self.label*T.dot(self.x1,self.x2) / (self.nx1*self.nx2) + T.dot(self.t1,self.x1) / (self.nx1*self.nt1)
        self.s2 = 1-self.label*T.dot(self.x1,self.x2) / (self.nx1*self.nx2) + T.dot(self.t2,self.x2) / (self.nx2*self.nt2)
        self.d1 = ifelse(T.lt(self.s1,self.zero), self.zero, self.s1)
        self.d2 = ifelse(T.lt(self.s2,self.zero), self.zero, self.s2)

        self.cost_part_one = self.d1 + self.d2
        self.cost_part_two = self.lam/2.*T.sum((self.We-self.initial_We)**2)

        self.grad_one = theano.function(inputs=[self.idx1, self.idx2, self.label], outputs = T.grad(self.cost_part_one, self.We))
        self.grad_two = theano.function(inputs=[self.lam], outputs = T.grad(self.cost_part_two, self.We))

        self.cost_1 = theano.function(inputs=[self.idx1, self.idx2, self.label], outputs = self.cost_part_one)
        self.cost_2 = theano.function(inputs=[self.lam], outputs = self.cost_part_two)

        self.updateAda = theano.function(inputs=[self.gg], updates=[( self.We_ada,  self.We_ada + (self.gg)**2)])
        self.updateWe = theano.function(inputs=[self.gg, self.eta], updates=[( self.We, self.We- self.eta*(self.gg)/(1E-6 + T.sqrt( self.We_ada)))])

    def updateBatchParams(self,d,p,eta,lam):
        gg = 0.
        for i in range(len(d)):
            (id1, id2, l) =d[i]
            idxs = (id1,id2)
            gg += self.grad_one(idxs,p[i],l)
        gg = gg/len(d)
        gg = gg + self.grad_two(lam)
        self.updateAda(gg)
        self.updateWe(gg,eta)

    def getBatchCost(self,d,p,lam):
        cost = 0
        for i in range(len(d)):
            #print d[i]
            (id1, id2, l) =d[i]
            idxs = (id1,id2)
            cost += self.cost_1(idxs,p[i],l)
        return cost/len(d) + self.cost_2(lam)

    def getWe(self):
        return self.We.get_value()