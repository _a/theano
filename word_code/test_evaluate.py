from utils import getWordmap
from params import params
from utils import getData
from evaluate import evaluate_adagrad
import numpy as np

hiddensize = 25
(words, We) = getWordmap('../data/skipwiki25.txt')
We=We/np.sqrt(np.power(We,2).sum(axis=1))
params = params()
examples = getData(params.dataf)
evaluate_adagrad(We,words)