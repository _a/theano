
from utils import getWordmap
from utils import lookup
from scipy.spatial.distance import cosine
from scipy.stats import spearmanr
import numpy as np
import math

def llm(p1,p2,words,We):
    p1 = p1.split()
    p2 = p2.split()
    total = 0
    for i in p1:
        v1 = lookup(We,words,i)
        max = 0
        for j in p2:
            v2 = lookup(We,words,j)
            score = -1*cosine(v1,v2)+1
            if(score > max):
                max = score
        total += max
    llm_score = 0.5*total / len(p1)
    total = 0
    for i in p2:
        v1 = lookup(We,words,i)
        max = 0
        for j in p1:
            v2 = lookup(We,words,j)
            score = -1*cosine(v1,v2)+1
            if(score > max):
                max = score
        total += max
    llm_score += 0.5*total / len(p2)
    return llm_score

def add(p1,p2,words,We):
    p1 = p1.split()
    p2 = p2.split()
    accumulator = np.zeros(lookup(We,words,p1[0]).shape)
    for i in p1:
        v = lookup(We,words,i)
        accumulator = accumulator + v
    p1_emb = accumulator / len(p1)
    accumulator = np.zeros(lookup(We,words,p2[0]).shape)
    for i in p2:
        v = lookup(We,words,i)
        accumulator = accumulator + v
    p2_emb = accumulator / len(p1)
    return -1*cosine(p1_emb,p2_emb)+1

def average(x):
    assert len(x) > 0
    return float(sum(x)) / len(x)

def pearson(x, y):
    assert len(x) == len(y)
    n = len(x)
    assert n > 0
    avg_x = average(x)
    avg_y = average(y)
    diffprod = 0
    xdiff2 = 0
    ydiff2 = 0
    for idx in range(n):
        xdiff = x[idx] - avg_x
        ydiff = y[idx] - avg_y
        diffprod += xdiff * ydiff
        xdiff2 += xdiff * xdiff
        ydiff2 += ydiff * ydiff
    
    if xdiff2 == 0 or ydiff2 == 0:
        return 0.0
    
    return diffprod / math.sqrt(xdiff2 * ydiff2)

def EvalSingleSystem(testlabelfile, sysscores):
    
    # read in golden labels
    goldlabels = []
    goldscores = []
        
    hasscore = False
    with open(testlabelfile) as tf:
        for tline in tf:
            tline = tline.strip()
            tcols = tline.split('\t')
            if len(tcols) == 2:
                goldscores.append(float(tcols[1]))
                if tcols[0] == "true":
                    goldlabels.append(True)
                elif tcols[0] == "false":
                    goldlabels.append(False)
                else:
                    goldlabels.append(None)

    tp = 0
    fn = 0
    # evaluation metrics
    for i in range(len(goldlabels)):
        
        if goldlabels[i] == True:
            tp += 1

    # system degreed scores vs golden binary labels
    # maxF1 / Precision / Recall
        
    maxF1 = 0
    P_maxF1 = 0
    R_maxF1 = 0
        
    # rank system outputs according to the probabilities predicted
    sortedindex = sorted(range(len(sysscores)), key = sysscores.__getitem__)
    sortedindex.reverse()
        
    truepos  = 0
    falsepos = 0

    for sortedi in sortedindex:
        if goldlabels[sortedi] == True:
            truepos += 1
        elif goldlabels[sortedi] == False:
            falsepos += 1
                
        precision = 0
                
        if truepos + falsepos > 0:
            precision = float(truepos) / (truepos + falsepos)
                
        recall = float(truepos) / (tp + fn)
        f1 = 0

        #print truepos, falsepos, precision, recall
                
        if precision + recall > 0:
            f1 = 2 * precision * recall / (precision + recall)
            if f1 > maxF1:
                maxF1 = f1
                P_maxF1 = precision
                R_maxF1 = recall
        
    # system degreed scores  vs golden degreed scores
    # Pearson correlation
    print len(sysscores), len(goldscores)
    pcorrelation = pearson(sysscores, goldscores)
    
    return (pcorrelation, maxF1)

file = open('../dataset/test.data','r')
lines = file.readlines()

(words, We) = getWordmap('../data/skipwiki25.txt')
#(words, We) = getWordmap('../../ppdb2/paragram_vectors.txt')

examples = []
llm_scores = []
add_scores = []
for i in lines:
    i =i.split("\t")
    s1 = i[2].lower()
    s2 = i[3].lower()
    examples.append((s1, s2))
    llm_scores.append(llm(s1,s2,words,We))
    add_scores.append(add(s1,s2,words,We))

print len(llm_scores), len(add_scores), len(examples)
print EvalSingleSystem("../dataset/test.label",llm_scores)
print EvalSingleSystem("../dataset/test.label",add_scores)

f = open('../dataset/ppdb-sample.tsv','r')
lines = f.readlines()

gold = []
llm_s = []
add_s = []
for i in lines:
    i=i.split("\t")
    score = float(i[0])
    p1 = i[2]
    p2 = i[3]
    gold.append(score)
    llm_s.append(llm(p1,p2,words,We))
    add_s.append(add(p1,p2,words,We))

print spearmanr(gold,llm_s), spearmanr(gold,add_s)